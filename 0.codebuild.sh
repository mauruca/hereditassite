#!/bin/bash
# Copyright (C) 2019 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.
echo " "
echo "Starting the build container..."
echo " "
docker run --rm -it --cpus=".10" -u $(id -u):$(id -g) -v $(pwd):/build hereditas/minifier:1.0 /bin/sh -c "cd /build;./_minify.sh"
cp -r wwwhereditasnetbr/css minwwwhereditasnetbr
cp -r wwwhereditasnetbr/js minwwwhereditasnetbr
cp -r wwwhereditasnetbr/font-awesome minwwwhereditasnetbr
cp -r wwwhereditasnetbr/fonts minwwwhereditasnetbr
cp -r wwwhereditasnetbr/img minwwwhereditasnetbr
cp -r wwwhereditasnetbr/less minwwwhereditasnetbr
cp wwwhereditasnetbr/404.html minwwwhereditasnetbr/404.html
cp wwwhereditasnetbr/500.html minwwwhereditasnetbr/500.html
cp wwwhereditasnetbr/LICENSE minwwwhereditasnetbr/LICENSE