# [Hereditas Product Web Site](http://www.hereditas.net.br/)

This page is based on [Creative](http://startbootstrap.com/template-overviews/creative/) is a one page creative theme for [Bootstrap](http://getbootstrap.com/) created by [Start Bootstrap](http://startbootstrap.com/).

## Getting Started

To use this theme, choose one of the following options to get started:
* Download the latest release on Start Bootstrap
* Fork this repository on GitHub
* No need to install node. It will user a hereditas/minifier image to do it.
* execute ./0.dockerbuild.sh to minify
* execute ./1.build.sh to build the site image
* execute ./2.push.sh to send the site image to hub

## Bugs and Issues

Have a bug or an issue with this theme? [Open a new issue](https://github.com/IronSummitMedia/startbootstrap-creative/issues) here on GitHub or leave a comment on the [template overview page at Start Bootstrap](http://startbootstrap.com/template-overviews/creative/).

## Creator
Hereditas was created by and is maintained by **Mauricio Pinheiro**, Managing Partner at **Hereditas**.

Hereditas is based on the [Bootstrap](http://getbootstrap.com/) framework created by [Mark Otto](https://twitter.com/mdo) and [Jacob Thorton](https://twitter.com/fat).

## Copyright and License
Copyright (C) 2015-2016 Mauricio Costa Pinheiro. Todos os direitos reservados.
Ver arquivo LICENSE para os detalhes.

Copyright 2013-2015 Iron Summit Media Strategies, LLC. Code released under the [Apache 2.0](https://github.com/IronSummitMedia/startbootstrap-creative/blob/gh-pages/LICENSE) license.
