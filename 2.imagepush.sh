#!/bin/bash
# Copyright (C) 2015-2016 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

tagversion="$(cat version)"
docker push hereditas/site:$tagversion
exit 0
