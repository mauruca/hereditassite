#!/bin/sh
# Copyright (C) 2015-2016 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

# HTML
rm -Rf minwwwhereditasnetbr
mkdir minwwwhereditasnetbr
echo "Index"
# nao usar --remove-empty-elements pois acaba com um efeito do site
html-minifier \
--collapse-boolean-attributes --collapse-whitespace \
--decode-entities --html5 \
--collapse-boolean-attributes --collapse-inline-tag-whitespace \
--prevent-attributes-escaping --process-conditional-comments \
--remove-attribute-quotes --remove-comments --remove-empty-attributes \
--remove-optional-tags --remove-redundant-attributes --remove-script-type-attributes \
--remove-style-link-type-attributes --remove-tag-whitespace --sort-attributes --sort-class-name \
--trim-custom-fragments --use-short-doctype \
-o minwwwhereditasnetbr/index_pre.html wwwhereditasnetbr/index_notmin.html
cat licensedisclaimer minwwwhereditasnetbr/index_pre.html >> minwwwhereditasnetbr/index.html
rm minwwwhereditasnetbr/index_pre.html

# CSS
mkdir minwwwhereditasnetbr/css
echo "creative.css"
html-minifier \
----minify-css \
--collapse-boolean-attributes --collapse-whitespace \
--decode-entities --html5 \
--collapse-boolean-attributes --collapse-inline-tag-whitespace \
--prevent-attributes-escaping --process-conditional-comments \
--remove-attribute-quotes --remove-comments --remove-empty-attributes --remove-empty-elements \
--remove-optional-tags --remove-redundant-attributes --remove-script-type-attributes \
--remove-style-link-type-attributes --remove-tag-whitespace --sort-attributes --sort-class-name \
--trim-custom-fragments --use-short-doctype \
-o minwwwhereditasnetbr/css/creative.min.css wwwhereditasnetbr/css_notmin/creative.css

echo "easyzoom.css"
html-minifier \
----minify-css \
--collapse-boolean-attributes --collapse-whitespace \
--decode-entities --html5 \
--collapse-boolean-attributes --collapse-inline-tag-whitespace \
--prevent-attributes-escaping --process-conditional-comments \
--remove-attribute-quotes --remove-comments --remove-empty-attributes --remove-empty-elements \
--remove-optional-tags --remove-redundant-attributes --remove-script-type-attributes \
--remove-style-link-type-attributes --remove-tag-whitespace --sort-attributes --sort-class-name \
--trim-custom-fragments --use-short-doctype \
-o minwwwhereditasnetbr/css/easyzoom.min.css wwwhereditasnetbr/css_notmin/easyzoom.css

echo "error.css"
html-minifier \
----minify-css \
--collapse-boolean-attributes --collapse-whitespace \
--decode-entities --html5 \
--collapse-boolean-attributes --collapse-inline-tag-whitespace \
--prevent-attributes-escaping --process-conditional-comments \
--remove-attribute-quotes --remove-comments --remove-empty-attributes --remove-empty-elements \
--remove-optional-tags --remove-redundant-attributes --remove-script-type-attributes \
--remove-style-link-type-attributes --remove-tag-whitespace --sort-attributes --sort-class-name \
--trim-custom-fragments --use-short-doctype \
-o minwwwhereditasnetbr/css/error.min.css wwwhereditasnetbr/css_notmin/error.css

echo "hereditas.css"
html-minifier \
----minify-css \
--collapse-boolean-attributes --collapse-whitespace \
--decode-entities --html5 \
--collapse-boolean-attributes --collapse-inline-tag-whitespace \
--prevent-attributes-escaping --process-conditional-comments \
--remove-attribute-quotes --remove-comments --remove-empty-attributes --remove-empty-elements \
--remove-optional-tags --remove-redundant-attributes --remove-script-type-attributes \
--remove-style-link-type-attributes --remove-tag-whitespace --sort-attributes --sort-class-name \
--trim-custom-fragments --use-short-doctype \
-o minwwwhereditasnetbr/css/hereditas.min.css wwwhereditasnetbr/css_notmin/hereditas.css

# JAVASCRIPT
mkdir minwwwhereditasnetbr/js
echo "creative.js"
html-minifier \
----minify-js \
-o minwwwhereditasnetbr/js/creative.min.js wwwhereditasnetbr/js_notmin/creative.js

echo "jquery.fittext.js"
html-minifier \
----minify-js \
-o minwwwhereditasnetbr/js/jquery.fittext.min.js wwwhereditasnetbr/js_notmin/jquery.fittext.js

echo "hotjar.js"
html-minifier \
----minify-js \
-o minwwwhereditasnetbr/js/hotjar.min.js wwwhereditasnetbr/js_notmin/hotjar.js