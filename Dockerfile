FROM nginx:alpine

LABEL author="Mauricio Costa Pinheiro"
LABEL maintainer="mauricio@hereditas.net.br"

# Copy web site
COPY ./minwwwhereditasnetbr /usr/share/nginx/html
